#include <string>
#include <cstdlib>
#include <iostream>
#include <sstream>

using namespace std ;


/*
String compression using Run-length encoding (RLE)
*/

string compress (const string &str)
{
	string output;
	stringstream ss;
	int count = 0;
	char c;

	for (int i = 0 ; i < str.length() ; i+=count)
	{
		count = 0;
		c = str.at(i);
		while (c == str.at(i + count)) 
		{
			++count;
			if(i + count >= str.length()) 
				break;
		}
		ss << count;
		output+=ss.str();
		output+=c;

		ss.str(string());
	}
	return output;//(output.size() < str.size() ? output : str);
}

int main(int argc , char **argv) {

	string inputString;
	cout << "Enter your string : ";
	cin >> inputString;

	string compressed = compress (inputString);

	cout << "RLE Compression: " << compressed << endl;

#ifdef _WIN32
	system("PAUSE");
#endif

	return 0;
}
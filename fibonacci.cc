#include <iostream>
#include <cstdlib>

using namespace std;


int fibonacci (const int& n){

	if (n == 0) return 0;
	if (n == 1 || n == 2) return 1;

	if (n > 2){
		int count = 3 , result;
		int buffer [2] = {1 , 1};

		while (count != n){
			buffer[0] = buffer[1];
			buffer[1] = fibonacci(count);
			++count;
		}
		return result = buffer[0] + buffer[1];
	}
}

int main(int argc , char ** argv){

	int n;
	cout << "What nth fibonacci number would you like to display ? ";
	cin >> n;

	int number = fibonacci(n);

	cout << "F(" << n << ") = " << number << endl; 

#ifdef _WIN32
	system ("pause");
#endif

	return 0;
}
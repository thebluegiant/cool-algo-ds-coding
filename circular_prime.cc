#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <cstdlib>

using namespace std;

/*
CIRCULAR PRIME
--------------
The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
How many circular primes are there below one million?
*/



bool isPrime (const int & number)
{
    if (number <= 3) {
        return number > 1;
    }
 
    if (number % 2 == 0 || number % 3 == 0) {
        return false;
    }
 
    for (int i = 5; i * i <= number; i += 6) {
        if (number % i == 0 || number % (i + 2) == 0) {
            return false;
        }
    }
 
    return true;
}

int main(int argc, char **argv)
{
	int n = 100; // default
	cout << "Enter your number : ";
	cin >> n;

	int count = 4; // 4 primes below 10

	stringstream convert;
	string sn;

	for (int i = 10; i < n ; ++i)
	{
		convert << i;
		sn = convert.str();
		sn += sn;

		// Checking if circular instances are prime
		int localCount = 0;
		for (int c = 0; c < 0.5 * sn.length(); ++c)
		{
			int circular = atoi(sn.substr(c, 0.5*sn.length()).c_str());
			if (isPrime(circular))
				++localCount;
			else break;
		}

		if (localCount == 0.5 * sn.length())
			++count;

		convert.str(string());
	}

	cout << "There is " << count << " circular primes below " << n << endl;

#ifdef _WIN32
   	system("PAUSE");
#endif

	return 0;
}
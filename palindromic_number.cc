#include <cstdlib>
#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

/*
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two n-digit numbers.
*/

int getNumberOfDigits (int integer)
{
	int digits = 0;
	for (digits = 0; integer > 0 ; ++digits)
		integer = integer*0.1;

	return digits;
}

int main (int argc, char **argv)
{
	int nDigits = 2; // default
	cout << "Number of target digits : ";
	cin >> nDigits;

	int lower_bound = pow(10, nDigits - 1);
	int upper_bound = pow(10, nDigits);

	stringstream convert;

	string tmpStr;
	int result = 0;
	int tmp = 0;
	int length = 0;
	int counter = 0;

	for (int i = lower_bound; i < upper_bound; ++i)
		for (int j = lower_bound; j < upper_bound; ++j)
		{
			tmp = i * j;
			length = getNumberOfDigits(tmp);
			if (length%2 == 1) continue;
			
			convert << tmp;
			tmpStr=convert.str();
			for (int c = 0 ; c < length * 0.5; ++c)
			{
				if (tmpStr.at(c) == tmpStr.at(length-1-c))
					++counter;
			}
			result = (counter == length * 0.5) ?  tmp : result;
			convert.str(string());
			counter = 0;
		}

#ifdef _WIN32
	system("pause");
#endif

	return 0;
}
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

void printing (vector<string>& file , const int& n) {

	if (file.size() <= n) {
		if (file.size() == 0) {
			cout << "Empty file" << endl;
			return;
		}

		for (int i = 0 ; i < file.size() ; ++i)
			cout << file[i] << endl; 
	 return;
	}

	vector<string>::iterator tmpIt = file.begin();
	while (distance(file.begin(), tmpIt) != file.size() - n)
		++tmpIt;

	cout << endl << "The " << n << " last lines are : " << endl;
	for (vector<string>::iterator it = tmpIt ; it < file.end() ; ++it){
		cout << *it << endl;
	}

}


int main(int argc  , char **argv) 
{
	vector<string> file;
	string word;
	int N, n;
	cout << "Enter file size : " ;
	cin  >> N;
	cout << "How many last lines would you like to print ? ";
	cin  >> n;

	for (int i = 0 ; i < N ; i++){
		cout << "Enter line #" << i << " : ";
		cin >> word;
		file.push_back(word);
	}
	cout << endl;

	printing(file , n);

#ifdef _WIN32
   	system("PAUSE");
#endif

	return 0;
}